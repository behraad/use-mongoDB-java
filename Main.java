package sql_test;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Iterator;

/**
 Created by ehsan on 11/27/17.
 */
public class Main {
    public static void main(String[] args) {
        MongoClient client = new MongoClient("localhost",27017);
        MongoDatabase db = client.getDatabase("mydb");
        MongoCollection<Document> docs = db.getCollection("aaa");
        MyClass myClass = new MyClass("ehsan","karimi");
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
        try {
            String json = writer.writeValueAsString(myClass);
            docs.insertOne(Document.parse(json));
            FindIterable<Document> founds = docs.find();
            Iterator<Document> iterator = founds.iterator();
            while (iterator.hasNext()){
                Document document = iterator.next();
                try {
                    MyClass mc = mapper.readValue(document.toJson(),MyClass.class);
                    System.out.println(mc);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
class MyClass implements Serializable {
    @JsonProperty("name")
    String name;
    @JsonProperty("lastname")
    String lastname;
    @JsonProperty("_id")
    MongoId id;

    public MyClass() {
    }

    public MyClass(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}