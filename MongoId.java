package sql_test;

import com.fasterxml.jackson.annotation.JsonProperty;
//every class for add to mongo should have this class
public class MongoId {
    @JsonProperty("$oid")
    String id;
}
